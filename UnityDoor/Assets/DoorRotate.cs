﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorRotate : MonoBehaviour {

    public GameObject door;
    GameObject myPlayer;
    public float targetRot = 90f;
    Vector3 closedRot;
    bool dooropen;
    public float speed = 5;
    public Mouselisten mouse;
    bool intrigger;
    bool click;
    public Image cursorimage;
   

    // Use this for initialization


    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");

        if (intrigger)
        {
            if (mouse.mousecursoron == false) 
            {

                if (cursorimage.enabled == false)
                {
                    cursorimage.enabled = true;
                }


            }
            
            if(intrigger && mouse.mouseclicked && click == false)
            {
                click = true;
                Doorinteract();



            }


        }



    }

    // Update is called once per frame
    void Update() {

    }


    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {

        }

        {
            Debug.Log("trigger entered");
            Doorinteract();
            cursorimage.enabled = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            Debug.Log("trigger left");
            Doorinteract();
            cursorimage.enabled = false;
        }
    }

    void Doorinteract()
    {
        Vector3 finishedRot;
        if (dooropen != true)
        {
            Vector3 Playerdir = door.transform.position - myPlayer.transform.position;
            float dot = Vector3.Dot(Playerdir, transform.forward);
            Debug.Log(dot);
            dooropen = true;

            if (dot > 0)
            {

                finishedRot = (new Vector3(closedRot.x,closedRot.y + targetRot, closedRot.z));

            }
            else
            {
                finishedRot = (new Vector3(closedRot.x,closedRot.y - targetRot, closedRot.z));

            }
        }
        else
        {
            finishedRot = (closedRot);
            dooropen = false;
        }
        StopCoroutine("DoorMotion");
        StartCoroutine("DoorMotion", finishedRot);
    }

    IEnumerator DoorMotion(Vector3 target)
    {
        while(Quaternion.Angle(door.transform.localRotation, Quaternion.Euler(target)) >= 0.02f)
        {
            door.transform.localRotation = Quaternion.Slerp(door.transform.localRotation, Quaternion.Euler(target), speed * Time.deltaTime);
            yield return null;
        }
        yield return null;

    }

}









