﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouselisten : MonoBehaviour {
    public bool mousecursoron;
    public bool mouseclicked;
    
    void onmouseup()

    {
        mouseclicked = true;
    }

    void onmousedown()

    {
        mouseclicked = false;
    }

    void onmouseenter()

    {
        if (mousecursoron == false)
        {
            mousecursoron = true;
        }
    }

    void onmouseexit()

    {
        mousecursoron = false;
    }



}
